﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public BoardManager boardManager;

    private int level = 1;

    public int playerFoodPoints = 100;
    [HideInInspector] public bool playersTurn = true;

    //Delay entre turnos
    public float turnDelay = 0.1f;
    //Lista de inimigos da fase
    private List<Enemy> enemies;
    //Verdadeiro se o inimigo estiver se movendo
    private bool enemiesMoving;

    /*Referencias ao GameObjects da UI*/

    //Tempo antes de iniciar
    public float levelStartDelay = 2f;

    //texto do level
    private Text levelText;

    //Imagem de background
    private GameObject levelImage;

    //Impede de o player se mova enquanto é exibido a tela "Dia X"
    private bool doingSetup;

    private void OnLevelWasLoaded(int index)
    {
        level++;
        InitGame();
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance !=null)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        enemies = new List<Enemy>();
      
        boardManager = GetComponent<BoardManager>();
        InitGame();
    }

    //Inicializa O JOGO (limpa a lista de inimigos e gera o level)
    void InitGame()
    {
        doingSetup = true;
        levelImage = GameObject.Find("Levelimage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Dia " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear();
        boardManager.SetupScene(level);
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    //Move os inimigos, um de  cada vez em sequencia
    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);

        if (enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }

        playersTurn = true;
        enemiesMoving = false;
    }

    private void Update()
    {
        if (playersTurn == true || enemiesMoving == true || doingSetup == true)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    }

    //Usado pelo inimigo para se colocar na lista de inimigos desse GameManager
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    //Deabilita o gameObject
    public void GameOver()
    {
        //Mostra quantos dias o jogador sobreviveu
        levelText.text = "Depois de " + level + " dias você morreu.";
        levelImage.SetActive(true);

        enabled = false;
    }
}
