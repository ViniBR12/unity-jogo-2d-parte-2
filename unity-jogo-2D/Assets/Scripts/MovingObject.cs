﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class MovingObject : MonoBehaviour
{
    //Tempo que uma unidade demora para se mover(em segundos).
    public float moveTime = 0.1f;
    //Camada que checa colisões enquanto nos movemos e determina se o espaço esta livre ou não.
    public LayerMask blockingLayer;

    // O Box Collider 2D desse game object.
    private BoxCollider2D boxCollider;
    //O Rigid Body 2D desse game object.
    private Rigidbody2D rb2d;
    // Para tornar o calculo do movimento mais eficiente.
    private float inverseMoveTime;

    //Membros "virtual" podem ter uma implementação diferente nas classes filha.
    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        rb2d = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / moveTime;
    }

    protected IEnumerator SmoothMovement(Vector3 end) 
    {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while (sqrRemainingDistance > float.Epsilon)
        {
            Vector3 newPosition = Vector3.MoveTowards(rb2d.position, end, inverseMoveTime * Time.deltaTime);
            rb2d.MovePosition(newPosition);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }
    //Detecta quando o player nao pode se mover
    protected abstract void OnCantMove<T>(T component)
        where T : Component;

    //Move o objeto
    protected bool Move (int xDir, int yDir, out RaycastHit2D hit)
    {
        //Posição atual
        Vector2 start = transform.position;
        //Posição final
        Vector2 end = start + new Vector2(xDir, yDir);

        boxCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        boxCollider.enabled = true;

        if (hit == false)
        {
            StartCoroutine(SmoothMovement(end));
            return true;
        }


        return false;
    }

    protected virtual void AttemptMove<T> (int xDir, int yDir) where T : Component 
    {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);

        if (hit.transform == null)
        {
            return;
        }

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
        {
            OnCantMove(hitComponent);
        }
    }
}
