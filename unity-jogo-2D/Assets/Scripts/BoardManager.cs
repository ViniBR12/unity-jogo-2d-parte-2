﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;
using Unity.Mathematics;

//Cria uma fase aleatório cada vez que uma nova se inicia, baseado no número do level.
public class BoardManager : MonoBehaviour
{
    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 8;
    public int rows = 8;

    public Count wallCount = new Count(5, 9);
    public Count foodCount = new Count(1, 5);

    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;
    public GameObject[] outerWallTiles;

    private Transform boardHolder;

    private List<Vector3> gridPosition = new List<Vector3>();

    //Preenche a lista com posições possíveis de se spawnar objetos.
    public void InitialiseList()
    {
        gridPosition.Clear();
        for (int x = 1; x < columns - 1; x++)
        {
            for (int y = 1; y < rows; y++)
            {
                gridPosition.Add(new Vector3(x, y, 0f));
            }
        }
    }

    //Cria as paredes externas do nosso level e o chão.
    public void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < columns + 1; x++)
        {
            for (int y = -1; y < rows + 1; y++)
            {
                //Cria um "chão" a cada interação do laço.
                GameObject toIstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                //Verifica se estamos na posição necessaria para criar a parede externa.
                if (x == -1 || x == columns || y == -1 || y== rows)
                {
                    toIstantiate = outerWallTiles[Random.Range(0, outerWallTiles.Length)];
                }

                GameObject instance = Instantiate(toIstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    //Seleciona uma posição aleatoria dentro da fase/level/grid.
    Vector3 RandomPosition()
    {
        //Um número aleatorio que servirá de indice.
        int randomIndex = Random.Range(0, gridPosition.Count);
        //A posiçao aleatoria.
        Vector3 randomPosition = gridPosition[randomIndex];
        //Remove a posição da lista de posições, para que nao crie objetos em posições iguais
        gridPosition.RemoveAt(randomIndex);
        //Retorna a posição selecionada.
        return randomPosition;
    }

    //Cria os objetos "interativos do jogo.
    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
    {
        //Quantos de um dado objeto serão criados.
        int objectCount = Random.Range(minimum, maximum + 1);
        for (int i = 0; i < objectCount; i++)
        {
            // Seleciona uma posição aleatoria 
            Vector3 randomPosition = RandomPosition();
            //Seleciona um Tile (Bloco,Sprite) aleatorio.
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            //Cria o TIle (Bloco,Sprite)
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    //Função que fará a inicialização do level.
    public void SetupScene(int level)
    {
        //Cria as parede externas.
        BoardSetup();
        //Limpa a lista de posições.
        InitialiseList();
        //Cria os objetos interativos.
        LayoutObjectAtRandom(wallTiles, wallCount.minimum, wallCount.maximum);
        LayoutObjectAtRandom(foodTiles, foodCount.minimum, foodCount.maximum);

        //Determina o numero de inimigos por fase.
        int enemyCount = (int)Math.Log(level, 2f);
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);

        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0f), Quaternion.identity); 
    }

}
